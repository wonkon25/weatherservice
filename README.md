<div id="top"></div>

# Weather Service
> Java console application



### General Information
A system that allows you to collect data from weather services.
Retrieves data based on longitude and latitude. Then it saves the averaged readings to the database.
### Built With
* [Java](https://www.oracle.com/pl/java/technologies/javase/jdk11-archive-downloads.html)
* [Maven](https://maven.apache.org/)
* [Hibernate](https://hibernate.org/)
* [H2 Database](https://www.h2database.com/html/main.html)



### Acknowledgements

- This project was inspired by  [Open Weather](https://openweathermap.org/)
<p align="right">(<a href="#top">back to top</a>)</p>
