package com.wonkon.weatherstackapi.model;

public class Location {
    private String country;
    private String name;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Location{" +
                "country='" + country + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
