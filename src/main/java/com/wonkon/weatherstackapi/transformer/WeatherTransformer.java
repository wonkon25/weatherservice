package com.wonkon.weatherstackapi.transformer;

import com.wonkon.weatherservice.WeatherDTO;
import com.wonkon.weatherstackapi.model.Weather;

public class WeatherTransformer {

    public static WeatherDTO toDTO(Weather weather) {
        WeatherDTO weatherDTO = new WeatherDTO();
        weatherDTO.setTemperature(weather.getCurrent().getTemperature());
        weatherDTO.setFeelsLikeTemperature(weather.getCurrent().getFeelsLike());
        weatherDTO.setPressure(weather.getCurrent().getPressure());
        weatherDTO.setHumidity(weather.getCurrent().getHumidity());
        weatherDTO.setWindSpeed(weather.getCurrent().getWindSpeed());
        weatherDTO.setWindDirection(weather.getCurrent().getWindDirection());
        weatherDTO.setCountry(weather.getLocation().getCountry());
        weatherDTO.setCity(weather.getLocation().getName());
        return weatherDTO;
    }
}
