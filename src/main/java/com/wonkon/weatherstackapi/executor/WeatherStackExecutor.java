package com.wonkon.weatherstackapi.executor;

import com.wonkon.http.ExtHttpClient;
import com.wonkon.weatherstackapi.model.Weather;

public class WeatherStackExecutor {
    private final ExtHttpClient extHttpClient;

    public WeatherStackExecutor(ExtHttpClient extHttpClient) {
        this.extHttpClient = extHttpClient;
    }

    public Weather fetchWeatherByCityName(String cityName) {
        String url = String.format("http://api.weatherstack.com/current?access_key=17ff09ba764b15dc96f5f8436421b30f&query=%s", cityName);
        return extHttpClient.get(url, Weather.class);
    }
}
