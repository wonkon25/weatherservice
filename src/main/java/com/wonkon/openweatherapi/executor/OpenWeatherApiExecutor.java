package com.wonkon.openweatherapi.executor;

import com.wonkon.openweatherapi.model.Weather;
import com.wonkon.http.ExtHttpClient;

import java.io.IOException;

public class OpenWeatherApiExecutor {

    private final ExtHttpClient extHttpClient;

    public OpenWeatherApiExecutor(ExtHttpClient extHttpClient) {
        this.extHttpClient = extHttpClient;
    }

    public Weather fetchWeatherByCityName(String name) throws IOException {
        String url = String.format(
                "http://api.openweathermap.org/data/2.5/weather?q=%s&appid=baa6ece140be0985d8bf8766fa381d1d&units=metric",
                name);
        return extHttpClient.get(url, Weather.class);
    }

    public Weather fetchWeatherByCityLatAndLong(double lat, double log) {
        String url = String.format(
                "http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=baa6ece140be0985d8bf8766fa381d1d&units=metric",
                lat, log);
        return extHttpClient.get(url, Weather.class);
    }
}
