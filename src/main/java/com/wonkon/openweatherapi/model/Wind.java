package com.wonkon.openweatherapi.model;

import com.google.gson.annotations.SerializedName;

public class Wind {
    private float speed;
    @SerializedName("deg")
    private int direction;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Wind{" +
                "speed=" + speed +
                ", direction=" + direction +
                '}';
    }
}
