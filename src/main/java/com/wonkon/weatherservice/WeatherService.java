package com.wonkon.weatherservice;

import java.util.List;
import java.util.stream.Collectors;

public class WeatherService {

    private final List<ExternalWeatherApiService> apiServices;
    private final WeatherRepository weatherRepository;

    public WeatherService(WeatherRepository weatherRepository,
                          List<ExternalWeatherApiService> externalWeatherApiServices) {
        this.apiServices = externalWeatherApiServices;
        this.weatherRepository = weatherRepository;
    }

    public WeatherDTO getAverageWeatherParams(String city) {
        List<WeatherDTO> weatherDTOS = weatherRepository.findByCity(city);
        return calculateWeatherAverageParams(weatherDTOS);
    }

    public WeatherDTO fetchWeatherByCityName(String cityName) {
        List<WeatherDTO> weathers = apiServices.stream()
                .map(service->service.fetchWeatherByCityName(cityName))
                .collect(Collectors.toList());
        WeatherDTO weather = calculateWeatherAverageParams(weathers);
        weatherRepository.add(weather);
        return weather;
    }

    private WeatherDTO calculateWeatherAverageParams(List<WeatherDTO> weathers) {
        WeatherDTO weather = new WeatherDTO();
        weather.setCity(weathers.get(0).getCity());
        weather.setCountry(weathers.get(0).getCountry());
        double averageTemperature = weathers.stream()
                .mapToDouble(w->w.getTemperature())
                .average().orElse(0);
        double averageFeelsLikeTemperatue = weathers.stream()
                .mapToDouble(w->w.getFeelsLikeTemperature())
                .average().orElse(0);
        double averageWindSpeed = weathers.stream()
                .mapToDouble(w->w.getWindSpeed())
                .average().orElse(0);
        double averageWindDirection = weathers.stream()
                .mapToInt(w->w.getWindDirection())
                .average().orElse(0);
        double averagePressure = weathers.stream()
                .mapToInt(w->w.getPressure())
                .average().orElse(0);
        double averageHumidity = weathers.stream()
                .mapToInt(w->w.getHumidity())
                .average().orElse(0);
        weather.setTemperature((float) averageTemperature);
        weather.setPressure((int) averagePressure);
        weather.setHumidity((int) averageHumidity);
        weather.setWindDirection((int) averageWindDirection);
        weather.setWindSpeed((float) averageWindSpeed);
        weather.setFeelsLikeTemperature((float) averageFeelsLikeTemperatue);
        return weather;
    }
}
